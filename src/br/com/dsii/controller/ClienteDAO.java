/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dsii.controller;

import br.com.dsii.model.Cliente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author diegomoraes
 */
public class ClienteDAO {

    Connection conexao = null;
    PreparedStatement pst = null;
    ResultSet rs = null;

    Conexao con = new Conexao();

    //Método para deletar Cliente
    public void deletar(Cliente cliente, JFrame jfcliente) {

        String sql = "delete from Cliente where cli_id = ?";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            pst.setInt(1, cliente.getId());

            if (JOptionPane.showConfirmDialog(jfcliente, "Deseja deletar?", "Atenção", JOptionPane.YES_NO_CANCEL_OPTION) == 0) {
                pst.execute();
                JOptionPane.showMessageDialog(jfcliente, "Deletado com sucesso!");
                con.desconector(conexao);
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(jfcliente, "Erro ao deletar: " + e);

        }

    }

    //Método para inserir Usuário
    public void inserir(Cliente cliente, JFrame jfcliente) {

        String sql = "insert into Cliente (cli_nome, cli_cpf, cli_data_nasc) values (?,?,?) ";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            pst.setString(1, cliente.getNome());
            pst.setString(2, cliente.getCpf());
            pst.setString(3, cliente.getDatanasc());
            

            pst.execute();

            JOptionPane.showMessageDialog(jfcliente, "Registro inserido com sucesso!");

            con.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfcliente, "Erro ao inserir: " + ex);

        }

    }

    //Método para alterar Usuário
    public void altera(Cliente cliente, JFrame jfcliente) {

        String sql = "update Cliente set cli_nome=?, cli_cpf=?, cli_data_nasc=?"
                + "where cli_id = ?";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            pst.setString(1, cliente.getNome());
            pst.setString(2, cliente.getCpf());
            pst.setString(3, cliente.getDatanasc());
            pst.setInt(4, cliente.getId());

            pst.execute();

            JOptionPane.showMessageDialog(jfcliente, "Alterado com Sucesso");
            con.desconector(conexao);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(jfcliente, "Erro ao alterar: " + e);
        }

    }

    public void consultarId(JTextField ed_pesq, JTable tbUsuario, JFrame jfusuario) {

        String sql = "Select cli_id as ID, cli_nome as Nome, cli_cpf as CPF, cli_data_nasc as Data_Nascimento from Cliente where cli_id = ?";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            pst.setInt(1, Integer.valueOf(ed_pesq.getText()));
            rs = pst.executeQuery();

            tbUsuario.setModel(DbUtils.resultSetToTableModel(rs));
            con.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfusuario, ex);
        }

    }

    public void consultarNome(JTextField ed_pesq, JTable tbUsuario, JFrame jfusuario) {

        String sql = "Select cli_id as ID, cli_nome as Nome, cli_cpf as CPF, cli_data_nasc as Data_Nascimento from Cliente where cli_nome like ?";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            pst.setString(1, ed_pesq.getText() + "%");
            rs = pst.executeQuery();

            tbUsuario.setModel(DbUtils.resultSetToTableModel(rs));
            con.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfusuario, ex);
        }

    }

}
