/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dsii.model;

/**
 *
 * @author diegomoraes
 */
public class ItemPedido {

    /**
     * @return the Livro_liv_id
     */
    public int getLivro_liv_id() {
        return Livro_liv_id;
    }

    /**
     * @param Livro_liv_id the Livro_liv_id to set
     */
    public void setLivro_liv_id(int Livro_liv_id) {
        this.Livro_liv_id = Livro_liv_id;
    }

    /**
     * @return the Pedido_ped_id
     */
    public int getPedido_ped_id() {
        return Pedido_ped_id;
    }

    /**
     * @param Pedido_ped_id the Pedido_ped_id to set
     */
    public void setPedido_ped_id(int Pedido_ped_id) {
        this.Pedido_ped_id = Pedido_ped_id;
    }
    
    private int Livro_liv_id;
    private int Pedido_ped_id;
    
}
