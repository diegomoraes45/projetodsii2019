/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dsii.controller;

import br.com.dsii.model.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author diegomoraes
 */
public class UsuarioDAO {

    Connection conexao = null;
    PreparedStatement pst = null;
    ResultSet rs = null;

    Conexao con = new Conexao();

    //Método para deletar Usuário
    public void deletar(Usuario usuario, JFrame jfusuario) {

        String sql = "delete from Usuario where usu_id = ?";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            pst.setInt(1, usuario.getUsu_id());

            if (JOptionPane.showConfirmDialog(jfusuario, "Deseja deletar?", "Atenção", JOptionPane.YES_NO_CANCEL_OPTION) == 0) {
                pst.execute();
                JOptionPane.showMessageDialog(jfusuario, "Deletado com sucesso!");
                con.desconector(conexao);
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(jfusuario, "Erro ao deletar: " + e);

        }

    }

    //Método para inserir Usuário
    public void inserirUsu(Usuario usuario, JComboBox jcomboPerfil, JFrame jfusuario) {

        String sql = "insert into Usuario (usu_nome, usu_login, usu_senha, usu_tipo) values (?,?,?,?) ";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            pst.setString(1, usuario.getUsu_nome());
            pst.setString(2, usuario.getUsu_login());
            pst.setString(3, usuario.getUsu_senha());
            pst.setString(4, usuario.getUsu_tipo().toString());

            pst.execute();

            JOptionPane.showMessageDialog(jfusuario, "Registro inserido com sucesso!");

            con.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfusuario, "Erro ao inserir: " + ex);

        }

    }

    //Método para alterar Usuário
    public void alteraUsu(Usuario usuario, JComboBox jcomboPerfil, JFrame jfusuario) {

        String sql = "update Usuario set usu_nome=?, usu_login=?, usu_senha=?, usu_tipo=?"
                + "where usu_id = ?";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            pst.setString(1, usuario.getUsu_nome());
            pst.setString(2, usuario.getUsu_login());
            pst.setString(3, usuario.getUsu_senha());
            pst.setString(4, usuario.getUsu_tipo());
            pst.setInt(5, usuario.getUsu_id());

            pst.execute();

            JOptionPane.showMessageDialog(jfusuario, "Alterado com Sucesso");
            con.desconector(conexao);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(jfusuario, "Erro ao alterar: " + e);
        }

    }

    public void consultarId(JTextField ed_pesq, JTable tbUsuario, JFrame jfusuario) {

        String sql = "Select usu_id as ID, usu_nome as Nome, usu_login as Login, usu_tipo as Perfil from Usuario where usu_id = ?";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            pst.setInt(1, Integer.valueOf(ed_pesq.getText()));
            rs = pst.executeQuery();

            tbUsuario.setModel(DbUtils.resultSetToTableModel(rs));
            con.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfusuario, ex);
        }

    }

    public void consultarNome(JTextField ed_pesq, JTable tbUsuario, JFrame jfusuario) {

        String sql = "Select usu_id as ID, usu_nome as Nome, usu_login as Login, usu_tipo as Perfil from Usuario where usu_nome like ?";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            pst.setString(1, ed_pesq.getText() + "%");
            rs = pst.executeQuery();

            tbUsuario.setModel(DbUtils.resultSetToTableModel(rs));
            con.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfusuario, ex);
        }

    }

}
