/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dsii.controller;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author diegomoraes
 */
public class Conexao {

    public static Connection conector() {
        Connection conexao = null;
        String driver = "com.mysql.jdbc.Driver";

        try {
            Class.forName(driver);
            conexao = DriverManager.getConnection("jdbc:mysql://localhost/svl", "root", "");
            System.out.println("Conectado com Sucesso!");
            return conexao;
        } catch (Exception e) {
            System.out.println("Erro ao conectar: " + e);
            return null;
        }

    }

    public void desconector(Connection con) {
        try {
            con.close();
            System.out.println("Desconctado com Sucesso!");
        } catch (Exception e) {
            System.out.println("Erro ao desconectar: " + e);
        }
    }

}
