/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dsii.controller;

import br.com.dsii.model.Livro;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author diegomoraes
 */
public class LivroDAO {

    Connection conexao = null;
    PreparedStatement pst = null;
    ResultSet rs = null;

    Conexao con = new Conexao();

    //Método para deletar Livro
    public void deletar(Livro livro, JFrame jflivro) {

        String sql = "delete from Livro where liv_id = ?";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            pst.setInt(1, livro.getLiv_id());

            if (JOptionPane.showConfirmDialog(jflivro, "Deseja deletar?", "Atenção", JOptionPane.YES_NO_CANCEL_OPTION) == 0) {
                pst.execute();
                JOptionPane.showMessageDialog(jflivro, "Deletado com sucesso!");
                con.desconector(conexao);
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(jflivro, "Erro ao deletar: " + e);

        }

    }

    //Método para inserir Livro
    public void inserir(Livro livro, JFrame jflivro) {

        String sql = "insert into Livro (liv_nome, liv_custo, liv_preco, liv_estoque) values (?,?,?,?) ";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            pst.setString(1, livro.getLiv_nome());
            pst.setDouble(2, livro.getLiv_custo());
            pst.setDouble(3, livro.getLiv_preco());
            pst.setDouble(4, livro.getLiv_estoque());
            

            pst.execute();

            JOptionPane.showMessageDialog(jflivro, "Registro inserido com sucesso!");

            con.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jflivro, "Erro ao inserir: " + ex);

        }

    }

    //Método para alterar Livro
    public void altera(Livro livro, JFrame jflivro) {

        String sql = "update Livro set liv_nome=?, liv_custo=?, liv_preco=?, liv_estoque=?"
                + "where liv_id = ?";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
             pst.setString(1, livro.getLiv_nome());
            pst.setDouble(2, livro.getLiv_custo());
            pst.setDouble(3, livro.getLiv_preco());
            pst.setDouble(4, livro.getLiv_estoque());
            pst.setInt(5, livro.getLiv_id());

            pst.execute();

            JOptionPane.showMessageDialog(jflivro, "Alterado com Sucesso");
            con.desconector(conexao);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(jflivro, "Erro ao alterar: " + e);
        }

    }

    public void consultarId(JTextField ed_pesq, JTable tbLivro, JFrame jflivro) {

        String sql = "Select liv_id as ID, liv_nome as Nome, liv_custo as Custo, liv_preco as Preço, liv_estoque as Estoque from Livro where liv_id = ?";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            pst.setInt(1, Integer.valueOf(ed_pesq.getText()));
            rs = pst.executeQuery();

            tbLivro.setModel(DbUtils.resultSetToTableModel(rs));
            con.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jflivro, ex);
        }

    }

    public void consultarNome(JTextField ed_pesq, JTable tblivro, JFrame jflivro) {

        String sql = "Select liv_id as ID, liv_nome as Nome, liv_custo as Custo, liv_preco as Preço, liv_estoque as Estoque from Livro where liv_nome like ?";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            pst.setString(1, ed_pesq.getText() + "%");
            rs = pst.executeQuery();

            tblivro.setModel(DbUtils.resultSetToTableModel(rs));
            con.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jflivro, ex);
        }

    }

}
