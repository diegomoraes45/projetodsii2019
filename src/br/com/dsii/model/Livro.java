/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dsii.model;

/**
 *
 * @author diegomoraes
 */
public class Livro {

    /**
     * @return the liv_id
     */
    public int getLiv_id() {
        return liv_id;
    }

    /**
     * @param liv_id the liv_id to set
     */
    public void setLiv_id(int liv_id) {
        this.liv_id = liv_id;
    }

    /**
     * @return the liv_nome
     */
    public String getLiv_nome() {
        return liv_nome;
    }

    /**
     * @param liv_nome the liv_nome to set
     */
    public void setLiv_nome(String liv_nome) {
        this.liv_nome = liv_nome;
    }

    /**
     * @return the liv_custo
     */
    public Double getLiv_custo() {
        return liv_custo;
    }

    /**
     * @param liv_custo the liv_custo to set
     */
    public void setLiv_custo(Double liv_custo) {
        this.liv_custo = liv_custo;
    }

    /**
     * @return the liv_preco
     */
    public Double getLiv_preco() {
        return liv_preco;
    }

    /**
     * @param liv_preco the liv_preco to set
     */
    public void setLiv_preco(Double liv_preco) {
        this.liv_preco = liv_preco;
    }

    /**
     * @return the liv_estoque
     */
    public int getLiv_estoque() {
        return liv_estoque;
    }

    /**
     * @param liv_estoque the liv_estoque to set
     */
    public void setLiv_estoque(int liv_estoque) {
        this.liv_estoque = liv_estoque;
    }
    
    private int liv_id;
    private String liv_nome;
    private Double liv_custo;
    private Double liv_preco;
    private int liv_estoque;
    
}
