/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dsii.controller;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTable;

/**
 *
 * @author diegomoraes
 */
public class Funcoes {
    
    //HABILITA OS BOTÕES
    public void habilitaBotoes1(JButton inserir, JButton editar, JButton apagar, JButton confirmar, JButton cancelar){
        
        inserir.setEnabled(true);
        editar.setEnabled(true);
        apagar.setEnabled(true);
        confirmar.setEnabled(false);
        cancelar.setEnabled(false);
        
    }
    
    //DESABILITA OS BOTOES
     public void habilitaBotoes2(JButton inserir, JButton editar, JButton apagar, JButton confirmar, JButton cancelar){
        
        inserir.setEnabled(false);
        editar.setEnabled(false);
        apagar.setEnabled(false);
        confirmar.setEnabled(true);
        cancelar.setEnabled(true);
        
    }
     
     //FUNÇÃO PARA AJUSTE DE DIMENSÃO DE COLUNA NA JTABLE
    public void ajustaTabelasPedido(JTable tab1, JTable tab2,JTable tab3){
        tab1.getColumnModel().getColumn(0).setMaxWidth(30);
        tab2.getColumnModel().getColumn(0).setMaxWidth(30);
        tab3.getColumnModel().getColumn(0).setMaxWidth(30);
        
    }
    
    //FUNÇÃO PARA CONVERSÃO DO FORMATO DE MOEDA 
    public void converteMoeda(String total, CarrinhoDAO carDAO, JFrame jVenda, JLabel lbl_total){
                
        DecimalFormat f = new DecimalFormat("#,##0.00", new DecimalFormatSymbols(new Locale("pt", "BR")));
        total=String.valueOf(f.format(carDAO.totalCarrinho(jVenda)));
        lbl_total.setText("R$"+total); 
        
    }
    
    
}
