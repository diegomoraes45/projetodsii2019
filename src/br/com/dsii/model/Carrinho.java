/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dsii.model;

/**
 *
 * @author diegomoraes
 */
public class Carrinho {

    /**
     * @return the car_id
     */
    public int getCar_id() {
        return car_id;
    }

    /**
     * @param car_id the car_id to set
     */
    public void setCar_id(int car_id) {
        this.car_id = car_id;
    }

    /**
     * @return the car_livroid
     */
    public int getCar_livroid() {
        return car_livroid;
    }

    /**
     * @param car_livroid the car_livroid to set
     */
    public void setCar_livroid(int car_livroid) {
        this.car_livroid = car_livroid;
    }

    /**
     * @return the car_livronome
     */
    public String getCar_livronome() {
        return car_livronome;
    }

    /**
     * @param car_livronome the car_livronome to set
     */
    public void setCar_livronome(String car_livronome) {
        this.car_livronome = car_livronome;
    }

    /**
     * @return the car_livropreco
     */
    public double getCar_livropreco() {
        return car_livropreco;
    }

    /**
     * @param car_livropreco the car_livropreco to set
     */
    public void setCar_livropreco(double car_livropreco) {
        this.car_livropreco = car_livropreco;
    }

    /**
     * @return the car_livrocusto
     */
    public double getCar_livrocusto() {
        return car_livrocusto;
    }

    /**
     * @param car_livrocusto the car_livrocusto to set
     */
    public void setCar_livrocusto(double car_livrocusto) {
        this.car_livrocusto = car_livrocusto;
    }

    /**
     * @return the car_livroestoque
     */
    public int getCar_livroestoque() {
        return car_livroestoque;
    }

    /**
     * @param car_livroestoque the car_livroestoque to set
     */
    public void setCar_livroestoque(int car_livroestoque) {
        this.car_livroestoque = car_livroestoque;
    }
    
    private int car_id;
    private int car_livroid;
    private String car_livronome;
    private double car_livropreco;
    private double car_livrocusto;
    private int car_livroestoque;
    
}
