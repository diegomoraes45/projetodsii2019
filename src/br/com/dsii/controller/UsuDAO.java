/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dsii.controller;

import br.com.dsii.model.Usuario;
import java.sql.*;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author diegomoraes
 */
public class UsuDAO {

    Connection conexao;
    PreparedStatement pst;
    ResultSet rs;

    String sql = "select * from Usuario where usu_login = ? and usu_senha=?";

    Conexao con = new Conexao();
    Usuario usu = new Usuario();

    public void Acesso(Usuario usuario, JFrame principal, JFrame login) {
        
        usu.setUsu_login(usuario.getUsu_login());
        usu.setUsu_senha(usuario.getUsu_senha());

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            pst.setString(1, usu.getUsu_login());
            pst.setString(2, usu.getUsu_senha());

            rs = pst.executeQuery();

            if (rs.next()) {

                JOptionPane.showMessageDialog(null, "Acesso Permitido!");
                principal.setVisible(true);
                login.setVisible(false);
                con.desconector(conexao);

            } else {
                JOptionPane.showMessageDialog(null, "Acesso Negado!");
                con.desconector(conexao);
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Ocorreu um erro: " + e);
        }

    }

}
