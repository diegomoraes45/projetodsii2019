/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dsii.controller;

import br.com.dsii.model.Carrinho;
import br.com.dsii.model.Livro;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author diegomoraes
 */
public class CarrinhoDAO {

    Connection conexao = null;
    PreparedStatement pst = null;
    ResultSet rs = null;

    Conexao con = new Conexao();

    //MÉTODO PARA DELETAR ITEM DO CARRINHO
    public void deletar(Carrinho car, JFrame jfvenda) {

        String sql = "delete from Carrinho where car_id = ?";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            pst.setInt(1, car.getCar_id());

            if (JOptionPane.showConfirmDialog(jfvenda, "Deseja deletar?", "Atenção", JOptionPane.YES_NO_CANCEL_OPTION) == 0) {
                pst.execute();
                JOptionPane.showMessageDialog(jfvenda, "Deletado com sucesso!");
                con.desconector(conexao);
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(jfvenda, "Erro ao deletar: " + e);

        }

    }

    //MÉTODO PARA INSERIR ITEM NO CARRINHO
    public void inserir(Livro livro, JFrame jfvenda) {

        String sql = "insert into Carrinho (car_livroid, car_livronome, car_livropreco) values (?,?,?) ";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            pst.setInt(1, livro.getLiv_id());
            pst.setString(2, livro.getLiv_nome());
            pst.setDouble(3, livro.getLiv_preco());

            pst.execute();

            con.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfvenda, "Erro ao inserir: " + ex);

        }

    }

    //MÉTODO PARA CONSULTA DE LIVRO
    public void consultarLivro(JTextField ed_pesq, JTable tbLivro, JFrame jfvenda) {

        String sql = "Select liv_id as ID, liv_nome as Nome, liv_preco as Preço from Livro where liv_nome like ?";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            pst.setString(1, ed_pesq.getText() + "%");
            rs = pst.executeQuery();

            tbLivro.setModel(DbUtils.resultSetToTableModel(rs));
            con.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfvenda, ex);
        }

    }
    
    //MÉTODO PARA CONSULTA DE CLIENTE NO CARRINHO
    public void consultarCliente(JTextField ed_pesq, JTable tbCliente, JFrame jfvenda) {

        String sql = "Select cli_id as ID, cli_nome as Nome, cli_cpf as CPF from Cliente where cli_nome like ?";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            pst.setString(1, ed_pesq.getText() + "%");
            rs = pst.executeQuery();

            tbCliente.setModel(DbUtils.resultSetToTableModel(rs));
            con.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfvenda, ex);
        }

    }

    //MÉTODO PARA CONSULTA DE ITENS NO CARRINHO
    public void consultarItensVenda(JTable tbItens, JFrame jfvenda) {

        String sql = "Select car_id as ID, car_livroid as Codigo, car_livronome as Nome, car_livropreco as Preco from Carrinho";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            tbItens.setModel(DbUtils.resultSetToTableModel(rs));
            con.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfvenda, ex);
        }

    }

    //EXCLUI TODOS OS REGISTROS DO CARRINHO QUANDO FINALIZAR COMPRA OU SAIR
    public void limparCarrinho(JFrame jfvenda) {

        String sql = "Truncate table Carrinho";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            pst.execute();

            con.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfvenda, ex);
        }

    }
    
    //MÉTODO PARA CALCULAR O VALOR TOTAL DE ITENS NO CARRINHO
    public Float totalCarrinho(JFrame jfvenda){
        
         String sql = "SELECT SUM(car_livropreco) as total from Carrinho;";
         float total=0;
         float totaldec=0;
        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            rs=pst.executeQuery();
            rs.first();
            totaldec = rs.getFloat("total");
            
            total = totaldec;
                        
            con.desconector(conexao);
            
           

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfvenda, ex);
        }
        
        return total;
    }

}
