/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dsii.controller;

import br.com.dsii.model.Carrinho;
import br.com.dsii.model.Cliente;
import br.com.dsii.model.ItemPedido;
import br.com.dsii.model.Livro;
import br.com.dsii.model.Pedido;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author diegomoraes
 */
public class VendaDAO {

    Connection conexao = null;
    PreparedStatement pst, pst2 = null;
    ResultSet rs, rs2 = null;

    Conexao con = new Conexao();

    //MÉTODO PARA INATIVAR PEDIDO
    public void inativarPedido(JFrame jfvenda, int codped) {

        String sql = "update Pedido set ped_ativo = 0 where ped_id = ?";
        

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            pst.setInt(1, codped);

            if (JOptionPane.showConfirmDialog(jfvenda, "Deseja Inativar o Pedido?", "Atenção", JOptionPane.YES_NO_CANCEL_OPTION) == 0) {
                pst.execute();
                JOptionPane.showMessageDialog(jfvenda, "Inativado com sucesso!");
                con.desconector(conexao);
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(jfvenda, "Erro ao inativar: " + e);

        }

    }

    //MÉTODO PARA INSERÇÃO DE PEDIDO
    public void inserirPed(Pedido pedido, JFrame jfvenda) {

        String sql = "insert into Pedido (ped_data, ped_cliente, ped_valor, ped_ativo) values (?,?,?,?) ";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            pst.setDate(1, pedido.getPed_data());
            pst.setInt(2, pedido.getPed_cliente());
            pst.setDouble(3, pedido.getPed_valor());
            pst.setInt(4,1);

            pst.execute();

            con.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfvenda, "Erro ao inserir: " + ex);

        }

    }



    //MÉTODO PARA TRANSFERIR ITEM DO CARRINHO PARA O PEDIDO
    public void inserirItemPedido(JFrame jfvenda, int codpedido) {

        String sql = "select * from Carrinho";
        String sql2 = "insert into Item_Pedido (Livro_liv_id, Pedido_ped_id) values (?,?) ";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();
            
           
              while (rs.next()) {

                pst2 = conexao.prepareStatement(sql2);
                pst2.setInt(1, rs.getInt("car_livroid"));
                pst2.setInt(2, codpedido);
                pst2.execute();

            }   
            

           

            con.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfvenda, "Erro ao inserir: " + ex);

        }

    }

    //MÉTODO PARA CONSULTA DE PEDIDO E VERIFICAÇÃO DE ID DO ÚLTIMO REGISTRO
    public int consultarPedidoInicial(JFrame jfvenda) {

        String sql = "Select * from Pedido";
        int codped=0;
        
        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            if (rs.next()) {
                
                rs.last();
                codped = (rs.getInt("ped_id"))+1;

            }else{
                codped = 1;
            }

            con.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfvenda, ex);
        }
         return codped;
    }

    //MÉTODO PARA CONSULTAR ITENS DE UM PEDIDO 
    public void consultarPedidoFinal(JTable tbConsultaPed, JFrame jfvenda, int codped) {

        String sql = "select itemped_id as ID, liv_nome as Nome, liv_preco as Preco from vw_consultaped where ped_id = ?";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            pst.setInt(1, codped);
            rs = pst.executeQuery();

            tbConsultaPed.setModel(DbUtils.resultSetToTableModel(rs));
            con.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfvenda, ex);
        }

    }
    
    //MÉTODO PARA CONSULTAR TODOS OS PEDIDOS
    public void consultarPedidoAll(JTable tbConsultaPed, JFrame jfvenda) {

       String sql = "Select ped_id as ID, ped_data as Data, ped_valor as Total from Pedido where ped_ativo = 1";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            tbConsultaPed.setModel(DbUtils.resultSetToTableModel(rs));
            con.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfvenda, ex);
        }

    }
    
    //MÉTODO PARA CONSULTAR PEDIDO POR ID
    public void consultarPedidoId(JTable tbConsultaPed, JFrame jfvenda, int codped) {

       String sql = "Select ped_id as ID, ped_data as Data, ped_valor as Total from Pedido where ped_ativo = 1 and ped_id = ?";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            pst.setInt(1, codped);
            rs = pst.executeQuery();

            tbConsultaPed.setModel(DbUtils.resultSetToTableModel(rs));
            con.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfvenda, ex);
        }

    }
    
     

}
